from app.app import app


@app.shell_context_processor
def make_shell_context():
    return dict(app=app)


def main():
    app.jinja_env.cache = {}
    app.run(debug=True)


@app.route('/api-map', methods=['GET'])
def help():
    from flask import jsonify
    """
    This resource returns a list with all endpoints used by the API for reference
    Quick way to check if the blueprints are working well for instance
    """
    func_list = {}
    for rule in app.url_map.iter_rules():
        if rule.endpoint != 'static':
            func_list[rule.rule] = app.view_functions[rule.endpoint].__doc__
    return jsonify(func_list)

def create_database():
    from app.blueprints.auth.models.identity_model import UserModel
    from app.blueprints.bank_transaction.models.transaction_model import TransactionModel
    from app.database import database

    print("initializing tables")
    database.create_tables([UserModel, TransactionModel], safe = True)

if __name__ == '__main__':
    create_database()
    main()
