# from app.utils.helpers import ____

import os
from flask import Flask
from json import JSONEncoder

def is_dev() -> bool:

    return os.environ.get('FLASK_ENV', default='development') == 'development'


def is_prod() -> bool:

    return os.environ.get('FLASK_ENV', default='development') == 'production'


def is_testing() -> bool:

    return os.environ.get('FLASK_ENV', default='development') == 'testing'


def peewee_sql_to_str(sql):
    """
    usage:
        from app.utils.helpers import peewee_sql_to_str
        peewee_sql_to_str(rows.sql())

    """
    return (sql[0] % tuple(sql[1]))


class Serializer(JSONEncoder):
    """
    Customized class that handle serialization of python object recursively

    usage:
        - json.dumps(cls=Serializer)
    """
    def default(self, o):
        if hasattr(o, '__dict__'):
            return o.__dict__
        elif hasattr(o, '__str__'):
            return o.__str__()
        else:
            o

