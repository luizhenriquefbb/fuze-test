# from app.utils.cast_handlers import

import json
from app.utils.helpers import Serializer
from datetime import datetime, date
from typing import Any, Union
from locale import currency, format_string

def string_to_datetime(string: str, format="%Y-%m-%d"):
    """
    Converte string para datetime
    """
    return datetime.strptime(string, format)

def datetime_to_string(date: datetime, format: str = "%Y-%m-%d") -> str:
    return date.strftime(format)

def date_to_datetime(date:date):
    return datetime(date.year, date.month, date.day)

def is_number(inp:Any) -> bool:
    try:
        val = float(inp)
        return True
    except (ValueError, TypeError):
        return False

def locale_currency(value, grouping=True):

    if value < 0:
        value = abs(value)
        return f"({currency(value, grouping=grouping)})"
    else:
        return f"{currency(value, grouping=grouping)}"

def percentage(value: int, point: int = 2) -> str:

    percentage = "%.2f" % (value * 100)

    return percentage + "%"

def zfill(value: Any, qtd_zeros: int) -> str:

    return f"{value}".zfill(qtd_zeros)

def serialize_custom_class(obj):
    return json.dumps(obj, cls=Serializer)
