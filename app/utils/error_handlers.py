"""
Default errors handlers
"""

from app.logger import EXCEPTIONS_LOGGER
from app.utils.rest_handlers import (http_bad_request,
                                     http_internal_server_error,
                                     http_not_found, http_unauthorized,
                                     http_unprocessable_entity)
from flask.blueprints import Blueprint
from flask_jwt_extended.exceptions import (NoAuthorizationError,
                                           RevokedTokenError)
from werkzeug.exceptions import (BadRequest, InternalServerError, NotFound,
                                 Unauthorized, UnprocessableEntity)
from schema import SchemaError



def default_configure_error_handlers(bp: Blueprint) -> Blueprint:

    # Regular exceptions
    bp.register_error_handler(Exception, _handle_exception)
    bp.register_error_handler(NoAuthorizationError,_handle_no_authorization_error)
    bp.register_error_handler(RevokedTokenError, _handle_revoked_token_exception)

    # HTTP Exceptions
    bp.register_error_handler(BadRequest.code, _handle_bad_request_error)
    bp.register_error_handler(NotFound.code, _handle_not_found_error)
    bp.register_error_handler(InternalServerError.code,_handle_internal_server_error)
    bp.register_error_handler(Unauthorized.code, _handle_unauthorized_error)
    bp.register_error_handler(UnprocessableEntity, _handle_unprocessable_entity_error)
    bp.register_error_handler(SchemaError, _handle_schema_server_error)

    return bp


# Regular exceptions
def _handle_exception(e: Exception):

    EXCEPTIONS_LOGGER.exception(e)
    return http_internal_server_error(str(e).lower(), None)


def _handle_no_authorization_error(e: NoAuthorizationError):

    EXCEPTIONS_LOGGER.exception(e)
    return http_bad_request(str(e).lower(), None)


def _handle_revoked_token_exception(e: RevokedTokenError):

    EXCEPTIONS_LOGGER.exception(e)
    return http_unauthorized(str(e).lower(), None)


# HTTP Exceptions
def _handle_bad_request_error(e: BadRequest):

    EXCEPTIONS_LOGGER.exception(e)
    return http_bad_request(e.description, None)


def _handle_unauthorized_error(e: Unauthorized):

    EXCEPTIONS_LOGGER.exception(e)
    return http_unauthorized(e.description, None)


def _handle_unprocessable_entity_error(e: UnprocessableEntity):

    EXCEPTIONS_LOGGER.exception(e)
    return http_unprocessable_entity(e.description, None)


def _handle_not_found_error(e: NotFound):

    EXCEPTIONS_LOGGER.exception(e)
    return http_not_found(e.description, None)


def _handle_internal_server_error(e: InternalServerError):

    EXCEPTIONS_LOGGER.exception(e)
    return http_internal_server_error(e.description, None)


def _handle_schema_server_error(e: SchemaError):

    EXCEPTIONS_LOGGER.exception(e)
    return http_internal_server_error(e.code, payload={"msg":"schema error"})
