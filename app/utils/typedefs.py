from typing import Any, Dict, Tuple

HTTPRestResponse = Tuple[str, int]
Payload = Dict[str, Any]

class Subscriptable():
    """
    Allows access attributes of an object with '[]'

    i.e:
        cls['attr'] = 'foo'
    """
    def __getitem__(self, key):
        return getattr(self, key)

    def __setitem__(self, key, value):
        return setattr(self, key, value)
