import os

# variavel que guarda o caminho absoluto do programa
DIR_PATH = os.path.join(os.path.dirname(
    os.path.abspath(__file__)), os.path.pardir)


from flask import Flask

from app.config.config import CONFIGS
from app.logger import CONNECTION_LOGGER


def configure_locale() -> None:

    from locale import LC_ALL, setlocale
    setlocale(LC_ALL, 'pt_BR.UTF-8')

def configure_vars(app: Flask, config = None) -> None:
    if config is None:
        app.config.from_object('app.config.config.CONFIGS')
    else:
        # Load config as parameter if passed.
        app.config.from_mapping(config)

# Extension configurators
def configure_cors(app: Flask) -> None:

    from flask_cors import CORS
    CORS(app, resources={r"*": {"origins": "*"}})


def configure_debug_toolbar(app: Flask) -> None:

    from flask_debugtoolbar import DebugToolbarExtension

    db_toolbar = DebugToolbarExtension()
    db_toolbar.init_app(app)


def configure_jwt_manager(app: Flask) -> None:

    from app.blueprints.auth import JWT
    JWT.init_app(app)


def configure_redis(app: Flask) -> None:

    from flask_redis import FlaskRedis

    url = CONFIGS.REDIS_URL

    if url is None:
        raise RuntimeError("Redis host not configured.")
    redis = FlaskRedis()
    redis.init_app(app)


# Blueprints register
def register_blueprints(app: Flask) -> None:

    from app.blueprints.auth import auth
    from app.blueprints.bank_transaction import bank_transaction

    blueprints = (
        { 'blueprint': auth,             'name': 'API Authentication', },
        { 'blueprint': bank_transaction, 'name': 'API Transactions',   },
    )

    for entry in blueprints:
        blueprint = entry['blueprint']
        app.register_blueprint(blueprint)
        name = entry['name']
        CONNECTION_LOGGER.info(f'Blueprint {name} configured succesfully.')

# App creator
def create_app(config=None, app_name="DashAPI") -> Flask:

    global app

    # Application instance
    app = Flask(
        app_name, template_folder='templates', static_folder=os.path.join(os.path.dirname(__file__), 'static'),
        instance_relative_config=True)

    # Configure app
    CONNECTION_LOGGER.info('Initializing app configuration.')
    CONNECTION_LOGGER.info('Configuring app environment.')
    configure_vars(app, config)
    CONNECTION_LOGGER.info('Configuring app locale.')
    configure_locale()

    # Configure extensions and modules.
    CONNECTION_LOGGER.info('Configuring app extensions and modules.')
    CONNECTION_LOGGER.info('Configuring Flask-CORS.')
    configure_cors(app)
    CONNECTION_LOGGER.info('Configuring Debug Toolbar.')
    configure_debug_toolbar(app)
    CONNECTION_LOGGER.info('Configuring JWT Manager.')
    configure_jwt_manager(app)
    CONNECTION_LOGGER.info('Configuring Redis.')
    configure_redis(app)
    # Register blueprints
    CONNECTION_LOGGER.info('Registering blueprints.')
    register_blueprints(app)
    CONNECTION_LOGGER.info('App successfully configured.')

    return app


app = create_app()
