from flask import Blueprint
from flask_cors import CORS
from flask_jwt_extended import JWTManager

from .loaders import configure_jwt_loaders
from .error_handlers import configure_error_handlers
from .routes import configure_routes


_URL_PREFIX = '/bank_transaction'
bank_transaction = Blueprint('bank_transaction', __name__, url_prefix=_URL_PREFIX)
JWT = JWTManager()

bank_transaction = configure_error_handlers(bank_transaction)
JWT = configure_jwt_loaders(JWT)
bank_transaction = configure_routes(bank_transaction)
CORS(bank_transaction)
