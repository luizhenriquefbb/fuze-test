import json

from app.blueprints.auth.controllers.auth_controller import AuthController
from app.blueprints.bank_transaction.controllers.bank_transactions import \
    BankTransactionController
from app.utils.cast_handlers import serialize_custom_class
from app.utils.rest_handlers import (http_internal_server_error,
                                     http_not_found, http_ok)
from flask import request
from flask_jwt_extended.view_decorators import jwt_required
import schema

from .base_view import BaseView


class BankTransactionsView(BaseView):

    def __init__(self):
        super(BaseView, self).__init__()

    @staticmethod
    @jwt_required
    def get():

        identity = AuthController.get_identity()

        controller = BankTransactionController()
        account = controller.get_account_info(identity=identity)

        if account:
            return http_ok(message="Account amount", payload=json.loads(serialize_custom_class(account)))
        else:
            return http_not_found(message="User or account not found", payload={})


    @staticmethod
    @jwt_required
    def make_transaction():

        # validate body
        expected_schema = schema.Schema({
            "user_to_id": schema.Use(int),
            "amount": schema.Use(float),
        }, ignore_extra_keys=True)

        body =  expected_schema.validate(request.json)

        identity = AuthController.get_identity()

        response = BankTransactionController.make_transaction(
            identity_from=identity,
            identity_to_id=body['user_to_id'],
            transaction=body['amount']
        )

        if response.success:
            return http_ok('transaction ok', payload={})

        else:
            return http_internal_server_error(message="Error", payload={"reasons": response.reasons})


    @staticmethod
    @jwt_required
    def deposit_withdraw():
        # validate body
        expected_schema = schema.Schema({
            "amount": schema.Use(float)
        }, ignore_extra_keys=True)

        body =  expected_schema.validate(request.json)

        identity = AuthController.get_identity()

        response = BankTransactionController.deposit_withdraw(
            identity=identity,
            amount=body['amount']
        )

        if response.success:
            return http_ok('transaction ok', payload={})

        else:
            return http_internal_server_error(message="Error", payload={"reasons": response.reasons})
