from app.blueprints.auth.models.entities.identity import Identity
from flask.views import MethodView
from flask_jwt_extended import (
    get_jwt_identity,
    jwt_required,
)

from app.utils.rest_handlers import http_bad_request

class BaseView(MethodView):

    def __init__(self):

        super().__init__()

    @staticmethod
    @jwt_required
    def _get_identity() -> Identity:

        identity = Identity(**get_jwt_identity())
        if not identity:
            return http_bad_request("missing identity in JWT token", None)
        return identity
