import json
from datetime import datetime, timedelta

from app.blueprints.auth.controllers.auth_controller import AuthController
from app.blueprints.bank_transaction.controllers.transactions_history_controller import \
    TransactionsHistoryController
from app.blueprints.bank_transaction.views.base_view import BaseView
from app.utils.cast_handlers import (datetime_to_string, is_number,
                                     serialize_custom_class,
                                     string_to_datetime)
from app.utils.rest_handlers import http_bad_request, http_ok
from flask import request
from flask_jwt_extended.view_decorators import jwt_required


class TransactionsHistoryView(BaseView):
    # 30 days ago
    DEFAULT_FROM_DATE = datetime_to_string(datetime.now() - timedelta(days=30), format="%Y-%m-%d")
    # today
    DEFAULT_TO_DATE = datetime_to_string(datetime.now(), format="%Y-%m-%d")

    @staticmethod
    @jwt_required
    def get_history():
        # get route params from url
        date_from = request.args.get("from", TransactionsHistoryView.DEFAULT_FROM_DATE)
        date_to = request.args.get("to", TransactionsHistoryView.DEFAULT_TO_DATE)
        count = request.args.get("count", None)
        skip = request.args.get("skip", None)

        date_from = string_to_datetime(string=date_from)
        date_to = string_to_datetime(string=date_to)
        count = int(count) if is_number(count) else None
        skip = int(skip) if is_number(skip) else None

        # validate dates
        if date_from > date_to:
            return http_bad_request(message="Error",
                                    payload={"reasons": ['date_to must be greater than date_from']})

        identity = AuthController.get_identity()

        history = TransactionsHistoryController.get_history(
            identity=identity,
            date_from=date_from,
            date_to=date_to,
            count=count,
            skip=skip
        )

        return http_ok(message="History", payload={"history": json.loads(serialize_custom_class(history))})


