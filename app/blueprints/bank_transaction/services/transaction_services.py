from dataclasses import dataclass, field
from typing import List


@dataclass
class IsValid:
    is_valid:bool
    reasons: List[str] = field(default_factory=list)

class TransactionServices():

    @staticmethod
    def is_transaction_valid(
            current_value_from: float,
            current_value_to: float,
            transaction: float) -> IsValid:
        """
        Validate a transaction

        Rules:
            - For now we only have one rule
            - account can not be negative
            - transaction can not be negative

        Args:
            current_value_from (float): amount of money logged user owns
            current_value_to (float): amount of money recipient owns
            transaction (float): amount of transaction. Can be positive or negative
        """

        is_valid = IsValid(is_valid=True)

        if current_value_from + transaction < 0:
            is_valid.is_valid = False
            is_valid.reasons.append("User does not have enough money")

        if current_value_to - transaction < 0:
            is_valid.is_valid = False
            is_valid.reasons.append("User does not have enough money")

        if transaction < 0:
            is_valid.is_valid = False
            is_valid.reasons.append("Transaction value can not be negative")

        return is_valid

    @staticmethod
    def is_deposit_valid(
            current_value: float,
            amount: float) -> IsValid:

        """
        Validate an amount

        If amount < 0 means an withdraw

        Rules:
            - account can not be negative
        """
        is_valid = IsValid(is_valid=True)

        if current_value + amount < 0:
            is_valid.is_valid = False
            is_valid.reasons.append("User does not have enough money")

        return is_valid