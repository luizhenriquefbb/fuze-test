from app.blueprints.auth.models.identity_model import UserModel
import peewee
from app.blueprints.bank_transaction.models.base_model import TimestampedModel


class TransactionModel(TimestampedModel):
    user_from = peewee.ForeignKeyField(
        model=UserModel,
        column_name="user_from_id",
        backref="transactions_as_sender",
        null=True,
        default=None
    )
    user_to = peewee.ForeignKeyField(
        model=UserModel,
        column_name="user_to_id",
        backref="transactions_as_recipient",
        null=True,
        default=None
    )
    amount = peewee.FloatField(null=False, default=0, constraints=[peewee.SQL("DEFAULT '0'")])

    class Meta():
        table_name = 'transactions'
