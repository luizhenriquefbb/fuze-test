from dataclasses import dataclass
from typing import List, Union

from peewee import JOIN
from app.blueprints.bank_transaction.models.transaction_model import TransactionModel
from app.blueprints.auth.models.identity_model import UserModel
from datetime import datetime

from app.blueprints.auth.models.entities.identity import Identity

@dataclass
class User():
    cpf:str
    user_name:str

@dataclass
class HistoryResponseRow:
    userFrom:User
    userTo:Union[User, None]
    amount:float
    date:datetime

class TransactionsHistoryController():

    @staticmethod
    def get_history(identity: Identity, date_from: datetime, date_to: datetime, count: int = None,
                    skip: int = None) -> List[HistoryResponseRow]:
        """
        Fetch history of transactions of an user

        Args:
            identity (Identity): [description]
            date_from (datetime): [description]
            date_to (datetime): [description]
            count (int, optional): [description]. Defaults to None.
            skip (int, optional): [description]. Defaults to None.
        """

        UserFrom = UserModel.alias()
        UserTo = UserModel.alias()

        rows = (
            TransactionModel
            .select(TransactionModel, UserFrom, UserTo)
            .join(UserFrom, on=(TransactionModel.user_from == UserFrom.id))
            .join_from(TransactionModel, UserTo, JOIN.LEFT_OUTER, on=(TransactionModel.user_to == UserTo.id))
            .where(
                ((UserFrom.id == identity.id) | (UserTo.id == identity.id))
                & (TransactionModel.created_at >= date_from)
                & (TransactionModel.created_at <= date_to)
            )
            .limit(count)
            .offset(skip)
        )


        response = []

        for row in rows:

            # UserTo can be None
            user_to = None if not row.user_to else User(
                user_name=row.user_to.user_name,
                cpf=row.user_to.cpf
            )

            response.append(HistoryResponseRow(
                userFrom=User(
                    user_name=row.user_from.user_name,
                    cpf=row.user_from.cpf
                ),
                userTo=user_to,
                amount=row.amount,
                date=row.created_at
            ))

        return response
