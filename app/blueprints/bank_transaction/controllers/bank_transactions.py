from app.blueprints.bank_transaction.models.transaction_model import TransactionModel
from app.blueprints.bank_transaction.services.transaction_services import TransactionServices
from dataclasses import dataclass, field
from typing import List, Union
from app.blueprints.auth.models.identity_model import UserModel
from app.blueprints.auth.models.entities.identity import Identity
from app.database import database
import peewee

@dataclass
class AccountInfoResponse:
    balance: float

@dataclass
class TransactionResponse:
    success:bool
    reasons:List[str] = field(default_factory=list)

class BankTransactionController():
    @staticmethod
    def get_account_info(identity:Identity) -> Union[None, AccountInfoResponse]:
        user:Union[UserModel, None] = UserModel.get_or_none(id=identity.id)

        if not user:
            return None

        return AccountInfoResponse(user.account_balance)

    @staticmethod
    def make_transaction(identity_from:Identity, identity_to_id:int, transaction:float) -> TransactionResponse:
        user_from:Union[UserModel, None] = UserModel.get_or_none(id=identity_from.id)
        user_to:Union[UserModel, None] = UserModel.get_or_none(id=identity_to_id)

        # Check if user exists
        if not user_from:
            return TransactionResponse(success=False, reasons=["user_from not found"])
        if not user_to:
            return TransactionResponse(success=False, reasons=["user_to not found"])

        # validate transaction:
        validation = TransactionServices.is_transaction_valid(
            current_value_from=user_from.account_balance,
            current_value_to=user_to.account_balance,
            transaction=transaction
        )

        if not validation.is_valid:
            return TransactionResponse(success=False, reasons=validation.reasons)

        user_from.account_balance += transaction
        user_to.account_balance -= transaction

        with database.atomic() as atc:
            try:
                user_from.save()
                user_to.save()

                TransactionModel.create(
                    user_from=user_from,
                    user_to=user_to,
                    amount=transaction,
                )

            except peewee.IntegrityError:
                atc.rollback()
                return TransactionResponse(
                    success=False,
                    reasons=['Something went wrong during an operation in database']
                )

            return TransactionResponse(success=True)

    @staticmethod
    def deposit_withdraw(identity:Identity, amount:float) -> TransactionResponse:
        user:Union[UserModel, None] = UserModel.get_or_none(id=identity.id)

        # Check if user exists
        if not user:
            return TransactionResponse(success=False, reasons=["user_from not found"])

        # validate transaction:
        validation = TransactionServices.is_deposit_valid(
            current_value=user.account_balance,
            amount=amount
        )

        if not validation.is_valid:
            return TransactionResponse(success=False, reasons=validation.reasons)

        user.account_balance += amount

        with database.atomic() as atc:
            try:
                user.save()

                TransactionModel.create(
                    user_from=user,
                    user_to=None,
                    amount=amount,
                )

            except peewee.IntegrityError:
                atc.rollback()

                return TransactionResponse(
                    success=False,
                    reasons=['Something went wrong during an operation in database']
                )

        return TransactionResponse(success=True)

