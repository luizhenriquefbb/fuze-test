from app.blueprints.bank_transaction.views.transactions_history_view import TransactionsHistoryView
from flask import Blueprint
from .views.bank_transactions_view import BankTransactionsView


def configure_routes(bp: Blueprint) -> Blueprint:

    bp.add_url_rule('/get_account', view_func=BankTransactionsView.get, methods=['GET'])
    bp.add_url_rule('/get_history', view_func=TransactionsHistoryView.get_history, methods=['GET'])
    bp.add_url_rule('/make_transaction', view_func=BankTransactionsView.make_transaction, methods=['POST'])
    bp.add_url_rule('/deposit_withdraw', view_func=BankTransactionsView.deposit_withdraw, methods=['POST'])

    return bp
