from typing import Any, Union

from app.utils.rest_handlers import http_unauthorized
from app.utils.typedefs import HTTPRestResponse
from flask import current_app
from flask_jwt_extended import JWTManager


def configure_jwt_loaders(jwt: JWTManager) -> JWTManager:

    jwt.expired_token_loader(_handle_expired_token)
    jwt.unauthorized_loader(_handle_unauthorized_token)
    jwt.token_in_blacklist_loader(_verify_token_revoked_status)
    return jwt


def _handle_expired_token() -> Union[str, HTTPRestResponse]:
    return http_unauthorized("token has expired", None)


def _handle_unauthorized_token(message: str) -> Union[str, HTTPRestResponse]:

    return http_unauthorized(message, None)


def _verify_token_revoked_status(decrypted_token: Any) -> bool:

    jti = decrypted_token['jti']
    redis_client = current_app.extensions['redis']
    entry = redis_client.get(jti)
    if not entry:
        return True
    return entry.decode('utf-8') == 'False'
