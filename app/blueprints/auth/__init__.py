from flask import Blueprint
from flask_cors import CORS
from flask_jwt_extended import JWTManager

from .loaders import configure_jwt_loaders
from .error_handlers import configure_error_handlers
from .routes import configure_routes


_URL_PREFIX = '/auth'
auth = Blueprint('auth', __name__, url_prefix=_URL_PREFIX)
JWT = JWTManager()

auth = configure_error_handlers(auth)
JWT = configure_jwt_loaders(JWT)
auth = configure_routes(auth)
CORS(auth)
