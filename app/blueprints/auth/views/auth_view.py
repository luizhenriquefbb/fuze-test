from app.blueprints.auth.services.auth_services import AuthServices
import schema
from app.blueprints.auth.controllers.auth_controller import AuthController
from app.blueprints.auth.models.entities.identity import Identity
from app.blueprints.auth.models.identity_model import UserModel
from app.utils.rest_handlers import (http_bad_request, http_created, http_internal_server_error,
                                     http_not_found, http_ok)
from app.utils.typedefs import HTTPRestResponse
from flask import request
from flask_jwt_extended import (get_jwt_identity, get_raw_jwt,
                                jwt_refresh_token_required, jwt_required)
import peewee

def _get_controller():

    return AuthController()


def _get_identity():

    identity = Identity(**get_jwt_identity())
    return identity


def login() -> HTTPRestResponse:

    if not request.json:
        return http_bad_request(message="No body set", payload={})

    body = request.json

    try:
        data = {
            'login': body['login'],
            'password': body['password']
        }
    except KeyError as exception:
        return http_bad_request(f"missing key {str(exception)} in request JSON body", None)

    resp = _get_controller().login(**data)

    if not resp:
        return http_not_found("bad login or password", None)
    access_token, refresh_token = resp
    return http_created("login successful", {
        'access_token': access_token,
        'refresh_token': refresh_token
    })

def test():
    return http_ok(message="test okay", payload={})

def register_user():
    # validate body

    expected_schema = schema.Schema({
        "user_name": schema.Use(str),
        "password": schema.Use(str),
        "cpf": schema.Use(str),
        schema.Optional("balance"): schema.Use(float),
    }, ignore_extra_keys=True)

    body =  expected_schema.validate(request.json)

    if body['balance'] < 0:
        return http_bad_request(message="User not created", payload={"reasons": ['Balance must be greater than 0']})

    try:
        created_user = UserModel.create(
            user_name=body['user_name'],
            cpf=body['cpf'],
            password=AuthServices.encrypt_string(body['password']),
            account_balance=body.get('balance', 0) or 0,
        )

        return http_created(message="User created", payload={"user_id": created_user.id})
    except peewee.IntegrityError as e:
        return http_internal_server_error(message="User not created", payload={"reasons": e.__str__()})



@jwt_required
def logout() -> HTTPRestResponse:

    jti = get_raw_jwt()['jti']
    _get_controller().logout(jti)
    return http_ok("successfully logged out", None)


@jwt_refresh_token_required
def refresh() -> HTTPRestResponse:

    identity = _get_identity()
    if not identity:
        return http_bad_request("missing identity in JWT token", None)
    resp = _get_controller().refresh(identity)
    return http_created("login refresh successful", {
        'access_token': resp
    })

@jwt_refresh_token_required
def revoke_refresh() -> HTTPRestResponse:

    jti = get_raw_jwt()['jti']
    _get_controller().revoke_refresh(jti)
    return http_ok("refresh token succesfully revoked", None)
