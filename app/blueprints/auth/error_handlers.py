from app.utils.error_handlers import default_configure_error_handlers

from flask.blueprints import Blueprint

def configure_error_handlers(bp: Blueprint) -> Blueprint:

    bp = default_configure_error_handlers(bp)

    return bp


