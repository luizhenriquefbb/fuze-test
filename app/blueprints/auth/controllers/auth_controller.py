import json
from typing import Optional, Tuple

from flask_jwt_extended.utils import get_jwt_identity

from app.blueprints.auth.models.entities.identity import Identity
from app.blueprints.auth.models.identity_model import UserModel
from app.blueprints.auth.services.auth_services import AuthServices
from app.utils.cast_handlers import serialize_custom_class
from flask import current_app
from flask_jwt_extended import (create_access_token, create_refresh_token,
                                get_jti)


class AuthController:

    def __init__(self):

        self.__redis_client = current_app.extensions['redis']

    def __generate_access_token(self, identity: Identity) -> str:

        access_token = create_access_token(
            identity=json.loads(serialize_custom_class(obj=identity)))
        access_jti = get_jti(
            encoded_token=access_token)
        access_expires = current_app.config['JWT_ACCESS_TOKEN_EXPIRES']
        self.__redis_client.set(
            access_jti, access_token, access_expires)
        return access_token

    def __generate_refresh_token(self, identity: Identity) -> str:

        refresh_token = create_refresh_token(
            identity=json.loads(serialize_custom_class(obj=identity)))
        refresh_jti = get_jti(
            encoded_token=refresh_token)
        refresh_expire = current_app.config['JWT_REFRESH_TOKEN_EXPIRES']
        self.__redis_client.set(refresh_jti, 'True', refresh_expire)
        return refresh_token

    def __revoke_jti(self, jti: str):

        self.__redis_client.set(jti, False)

    @staticmethod
    def get_identity():
        identity = Identity(**get_jwt_identity())
        return identity


    def login(self, login: str, password: str) -> Optional[Tuple[str, str]]:

        dao = UserModel()

        identity_row = dao.get_or_none(
            cpf=login,
            password=AuthServices.encrypt_string(secret=password)
        )

        if not identity_row:
            return None

        identity = Identity(
            id=identity_row.id,
            user_name=identity_row.user_name,
            cpf=identity_row.cpf,
            password=identity_row.password,
            account_balance=identity_row.account_balance,
        )
        return (self.__generate_access_token(identity),
                self.__generate_refresh_token(identity))


    def logout(self, jti: str):

        self.__revoke_jti(jti)

    def refresh(self, identity: Identity) -> str:

        return self.__generate_access_token(identity)

    def revoke_refresh(self, jti: str):

        self.__revoke_jti(jti)
