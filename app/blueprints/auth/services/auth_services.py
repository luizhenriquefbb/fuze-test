from app.config.config import CONFIGS

class AuthServices():
    _key = CONFIGS.SECRET_KEY

    @staticmethod
    def encrypt_string(secret:str) -> str:
        """
        Fake cryptography. Just adding a prefix and a postfix. It was just to show you need to
        worry about this!!!
        @Parameters:
            - secret [string]

        @returns:
            - [string] encripted
        """

        encrypted_text = f"fake_encrypt-{secret}-fake_encrypt"

        return encrypted_text
