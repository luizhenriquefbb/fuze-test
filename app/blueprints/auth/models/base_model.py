import datetime
import peewee
from playhouse.shortcuts import model_to_dict
from app.database import database


class BaseModel(peewee.Model):
    id = peewee.AutoField(primary_key=True, unique=True)

    @staticmethod
    def get_view(obj_as_dict: dict):
        return obj_as_dict

    def asdict(self):
        return self.get_view(model_to_dict(self))

    class Meta():
        # Connect to a MySQL database on network.
        database = database

class TimestampedModel(BaseModel):
    # created_at = peewee.DateTimeField(default=datetime.datetime.now)
    created_at = peewee.DateTimeField(
        default=datetime.datetime.now,
        constraints=[peewee.SQL("DEFAULT 'CURRENT_TIMESTAMP'")])


