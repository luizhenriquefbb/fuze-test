
import peewee
from app.blueprints.auth.models.base_model import TimestampedModel


class UserModel(TimestampedModel):
    user_name = peewee.CharField(null=False)
    cpf = peewee.CharField(null=False, unique=True)
    password = peewee.CharField(null=True)
    account_balance = peewee.FloatField(null=False, constraints=[peewee.SQL("DEFAULT 0")])

    class Meta:
        table_name = 'users'


