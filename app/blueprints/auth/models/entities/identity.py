from dataclasses import (
    asdict,
    astuple,
    dataclass,
)


@dataclass
class Identity:

    id:int
    user_name:str
    cpf:str
    password:str
    account_balance:float

    def to_dict(self) -> dict:

        return asdict(self)

    def to_tuple(self) -> tuple:

        return astuple(self)
