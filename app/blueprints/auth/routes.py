from flask import Blueprint
from .views.auth_view import login, test, register_user


def configure_routes(bp: Blueprint) -> Blueprint:

    bp.add_url_rule('/test', view_func=test, methods=['GET'])
    bp.add_url_rule('/login', view_func=login, methods=['POST'])
    bp.add_url_rule('/create_user', view_func=register_user, methods=['PUT'])

    return bp
