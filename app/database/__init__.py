import os

import peewee
from app.app import DIR_PATH

# using sqlite
database = peewee.SqliteDatabase(
    database=os.path.join(DIR_PATH, "app", "database", "development_db.sqlite")
)

# using sql
# database = peewee.MySQLDatabase(
#     database=CONFIGS.MYSQL_SETTINGS['database'],
#     user=CONFIGS.MYSQL_SETTINGS['user'],
#     password=CONFIGS.MYSQL_SETTINGS['password'],
#     host=CONFIGS.MYSQL_SETTINGS['host'],
#     port=CONFIGS.MYSQL_SETTINGS['port'],
# )
