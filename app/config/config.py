"""
This file handle environment variables

DO NOT PUSH PRODUCTION ENVIRONMENT TO GIT. Only local development
"""
from datetime import timedelta
from typing import Type, Union
from app.utils.helpers import is_dev, is_prod, is_testing


def get_os_system():
    # Linux: Linux
    # Mac: Darwin
    # Windows: Windows
    import platform
    return platform.system()


class Default:

    # App
    DEBUG = True
    ENV = 'development'
    VERSION = '1.0.0'
    SECRET_KEY = 'dev_key_fuzecc'

    # Debug Toolbar
    DEBUG_TB_ENABLED = True
    DEBUG_TB_PROFILER_ENABLED = True
    DEBUG_TB_TEMPLATE_EDITOR_ENABLED = True
    DEBUG_TB_PANELS = ('flask_debugtoolbar.panels.versions.VersionDebugPanel',
                       'flask_debugtoolbar.panels.timer.TimerDebugPanel',
                       'flask_debugtoolbar.panels.headers.HeaderDebugPanel',
                       'flask_debugtoolbar.panels.request_vars.RequestVarsDebugPanel',
                       'flask_debugtoolbar.panels.config_vars.ConfigVarsDebugPanel',
                       'flask_debugtoolbar.panels.template.TemplateDebugPanel',
                       'flask_debugtoolbar.panels.logger.LoggingPanel',
                       'flask_debugtoolbar.panels.route_list.RouteListDebugPanel',
                       'flask_debugtoolbar.panels.profiler.ProfilerDebugPanel',)
    # JWT Manager
    JWT_SECRET_KEY = 'dev_key_fuzecc'
    JWT_TOKEN_LOCATION = ['headers', ]
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(days=1)
    JWT_REFRESH_TOKEN_EXPIRES = timedelta(days=30)
    JWT_ERROR_MESSAGE_KEY = 'message'
    JWT_IDENTITY_CLAIM = 'sub'
    JWT_BLACKLIST_ENABLED = True
    JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh', ]

    MYSQL_SETTINGS = {
        'host': '',
        'port': 3306,
        'database': 'localhost',
        'user': 'user',
        'password': '',
        'charset': 'utf8',
        'cursor_class': 'DictCursor',
        'use_unicode': True,
    }

    # Redis
    REDIS_URL = 'redis://fuze-test-redis:2dTJH43sB9@localhost:6380/0'

    BASE_URL = 'http://localhost:5010'


class Development(Default):
    DEBUG = True
    ENV = 'developing'


class Testing(Default):
    DEBUG = False
    ENV = 'testing'


# this class will never be use from this file.
# the real one is on .gitignore
class Production(Default):
    DEBUG = False
    ENV = 'production'


CONFIGS: Union[Type[Development], Type[Production], Type[Testing]]


def configure_vars() -> None:

    global CONFIGS

    # Load default config for the environment specified,
    # then overrides with environment specific configuration
    if is_dev():
        CONFIGS = Development
    elif is_prod():
        from .production import Production
        CONFIGS = Production
    elif is_testing():
        CONFIGS = Testing
    else:
        raise RuntimeError(
            "Unknown environment. Please configure the correct environment and restart the application.")


if __name__ != "__main__":
    configure_vars()
