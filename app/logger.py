#!/usr/bin/python
# -*- coding: utf-8 -*-

from app.app import DIR_PATH
from app.utils.helpers import is_dev, is_prod, is_testing
import logging
import os
from logging.handlers import RotatingFileHandler

class LogManager:
    def __init__(self, log_name,
                 log_format='%(asctime)s:%(name)s:%(levelname)s - %(message)s',
                 folder='logs/', maxSize=20, backupCount=5, log_level=None):
        """
        Constructor.

        Parameters
        ----------
            log_name : string
                log file name.
            log_format : string
                Log formatter.
            folder : string
                Folder of storage log files.
            maxSize : int
                Maximum file size (in megabytes).
            backupCount : int
                Backup of the last 5 files.logger_level = logging.DEBUG
        """
        self.log_name = log_name

        if not log_level:
            logger_level = logging.DEBUG
            if is_dev():
                logger_level = logging.DEBUG
            elif is_testing():
                logger_level = logging.INFO
            elif is_prod():
                logger_level = logging.ERROR


        # Logger
        self.logger = logging.getLogger(log_name)
        self.logger.setLevel(logger_level)

        # Checks if storage folder exists.
        if not os.path.isdir(folder):
            os.makedirs(folder)

        # Creates logging handlers
        # "_" + time.strftime("%d-%m-%y_%H:%M:%S") + ".log"
        filename = os.path.join(folder, log_name) + ".log"

        # Defines logging format
        formatter = logging.Formatter(log_format)

        # The rotating file handler limit file to 100MB and creates backups of the last 5 files.
        file_handler = RotatingFileHandler(
            filename, mode='a', maxBytes=maxSize*1024*1024,
            backupCount=backupCount, encoding=None, delay=False)
        file_handler.setFormatter(formatter)
        file_handler.setLevel(logger_level)

        # Channel handler
        channel_handler = logging.StreamHandler()
        channel_handler.setLevel(logger_level)
        channel_handler.setFormatter(formatter)

        # Add handlers to logger
        self.logger.addHandler(file_handler)
        self.logger.addHandler(channel_handler)

        self._mongoCollection = None

    def get_collection(self):

        # if self._mongoCollection:
        #     pass
        # else:
        #     from app.database.local_mongodb import connector
        #     _, get_database = connector()

        #     self._mongoCollection = get_database()[self.log_name]
        # return self._mongoCollection
        pass


    def info(self, message):
        """
        Logging info messages.

        Parameters
        ----------
            message : string
                Message to print and store in log file.
        """
        self.logger.info(message)

    def error(self, message):
        """
        Logging error messages.

        Parameters
        ----------
            message : string
                Message to print and store in log file.
        """
        self.logger.error(message)

    def debug(self, message):
        """
        Logging debug messages.

        Parameters
        ----------
            message : string
                Message to print and store in log file.
        """
        if is_dev():
            self.logger.debug(message)

    def warning(self, message):
        """
        Logging warning messages.

        Parameters
        ----------
            message : string
                Message to print and store in log file.
        """
        self.logger.warning(message)

    def exception(self, message:Exception):
        """
        Logging exception messages.

        Parameters
        ----------
            message : string
                Message to print and store in log file.
        """

        self.logger.exception(message)

        # collection = self.get_collection()
        # collection.insert_one({
        #     "message": message.__str__(),
        #     "time": datetime.now()
        # })

EXCEPTIONS_LOGGER = LogManager(log_name="exceptions")
CONNECTION_LOGGER = LogManager(log_name="connection_logger",
                               folder=os.path.join(DIR_PATH, 'logs/'))
LOGGER =            LogManager(log_name="logger",
                               folder=os.path.join(DIR_PATH, 'logs/'))



if is_dev():
    # configure peewee logger
    logger = logging.getLogger('peewee')
    logger.addHandler(logging.StreamHandler())
    logger.setLevel(logging.DEBUG)
