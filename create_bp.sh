# Create new bp
newBP=auth
mkdir app/blueprints/$newBP


mkdir app/blueprints/$newBP/controllers
mkdir app/blueprints/$newBP/models
mkdir app/blueprints/$newBP/entities
mkdir app/blueprints/$newBP/repositories
mkdir app/blueprints/$newBP/services
mkdir app/blueprints/$newBP/views


cat docs/create_bp/__init__.txt        > app/blueprints/$newBP/__init__.py
cat docs/create_bp/routes.txt          > app/blueprints/$newBP/routes.py
cat docs/create_bp/errors_handlers.txt > app/blueprints/$newBP/error_handlers.py
cat docs/create_bp/loaders.txt         > app/blueprints/$newBP/loaders.py

cat docs/create_bp/views/base_view.txt  > app/blueprints/$newBP/views/base_view.py
cat docs/create_bp/views/renomeie.txt   > app/blueprints/$newBP/views/renomeie.py

