<div style="text-align:center">
    <img src="https://fuze.cc/website_assets/img/logo.png" />
</div>

# The challenge

Make an API to simulate a bank system. The API must have endpoints for the following actions:

- Create user account
    - Receive Name, CPF and password
    - Return an OAUTH token to be used for account operations

- log in
    - Log in with CPF and password
    - Return an OAUTH token to be used for account operations

- Make credit operation on the account
    - Only with valid token

- Debit account operation
    - Only with valid token

- See account balance
    - Only with valid token

- See list of transactions carried out for this account (Credits and debits)
    - Only with valid token

###  EXTRA
- **Transaction between users registered in this Bank**

## Infrastructure

- python
- Docker
    - to easily create databases like redis and sql
- Flask
    - as a framework to build our API
- redis
    - a db to handle auth
- peewee
    - an orm to handle relational databases
- sqlite
- mysql


## Installation

```sh
# Create a virtual environment to install libraries
python3 -m venv venv

# activate environment
source venv/bin/activate

# Install libraries from txt file
pip3 install -r requirements.txt

```

## Docker

```sh
docker-compose build

docker-compose up -d
```

## How to execute it
```sh
FLASK_ENV="development" FLASK_APP="manage.py" python3 -m flask run --port=5010
```

```sh
FLASK_ENV="testing" FLASK_APP="manage.py" python3 -m flask run --port=5010
```

```sh
FLASK_ENV="production" FLASK_APP="manage.py" python3 -m flask run --port=5010
```

## How to test it

You can download all the routes environment variables and upload into [postman](https://www.postman.com/downloads/).



### How to import the data

![1](./readme_files/postman-routes-1.png)
![2](./readme_files/postman-routes-2.png)

- find `./docs/postman_routes/routes.json`

### How to set environments variables

![1](./readme_files/postman-import_variables-1.png)
![2](./readme_files/postman-import_variables-2.png)
![2](./readme_files/postman-import_variables-3.png)

- find `./docs/postman_routes/variables.json`

### How to see examples

![1](./readme_files/postman-examples-1.png)
![2](./readme_files/postman-examples-2.png)




# Projects' Architecture

![architecture](./readme_files/fuze_architecture.png)


# How to improve this code

- [ ] Use migrations to handle database changes
- [ ] Unit tests
- [ ] Integrity tests